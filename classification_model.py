import pandas as pd
import numpy as np
import warnings
warnings.filterwarnings('ignore')
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression
from sklearn.datasets import load_boston 
from sklearn.metrics import r2_score,mean_squared_error
X, y = load_boston(return_X_y=True)
X = pd.DataFrame(X)
y = pd.DataFrame(y)

from sklearn.model_selection import train_test_split
X_train,X_test,y_train,y_test = train_test_split(X,y,test_size=0.20,random_state=42)

# RandomForest Model
model = RandomForestRegressor()  
model.fit(X_train,y_train)
rf_pred = model.predict(X_test)

print("RandomForest-Model_predictions:",r2_score(rf_pred,y_test))
print("-----------------------------------------")
print("RandomForest-Mean Squared Error:",np.sqrt(mean_squared_error(rf_pred,y_test)))

# Linear Regression Model
model = LinearRegression()
model.fit(X_train,y_train)
lr_pred = model.predict(X_test)

print("LinearRegression-Model_predictions:",r2_score(lr_pred,y_test))
print("-----------------------------------------")
print("LinearRegression-Mean Squared Error:",np.sqrt(mean_squared_error(lr_pred,y_test)))




